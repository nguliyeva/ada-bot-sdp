#include <NewPing.h>

#include <SoftwareSerial.h>
SoftwareSerial Blue(2, 3);

#define TRIGGER_PIN  13  
#define ECHO_PIN     11

NewPing sonar(TRIGGER_PIN, ECHO_PIN); 
int LED_right = 8;
int LED_left = 12;

int IN1 = 4;           
int IN2 = 5;           
int IN3 = 6;         
int IN4 = 7;  

const int ENA = 9;
const int ENB = 10;

void setup() 
{
  Serial.begin(9600); 
  Blue.begin(9600);
  pinMode(LED_right,OUTPUT);
  pinMode(LED_left,OUTPUT);
 
  
  
}

void loop() 
{
   int distance = sonar.ping_cm();
   Serial.println(distance);
     if (distance<20){
        stop();
        Blue.write("a");
    }
    
    char data;
  
  if(Blue.available()) {
    
    data=Blue.read();
    Serial.println(data);

    
    
    if(data=='f' )
    {
       forward();
      digitalWrite(13,HIGH);
      digitalWrite(8,LOW);
      digitalWrite(12,LOW); 
    }

    
    else if(data=='b')
    {
      digitalWrite(13,LOW);
      digitalWrite(8,LOW);
      digitalWrite(12,LOW);
      forward();
      right();
      forward();
      left();
      
    }

   else if(data=='r')
    {
      digitalWrite(8,HIGH);
      digitalWrite(12,LOW);
      digitalWrite(13,LOW);
      right();
    }

    else if(data=='l')
    {
      digitalWrite(12,HIGH);
      digitalWrite(8,LOW);
      digitalWrite(13,LOW);
      left();
    }

    
  }

  
}

void forward(){
   //Serial.println("forward");
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    analogWrite(ENA, 150);
    analogWrite(ENB, 150);

}


void stop()
{
   //Serial.println("stop");
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);
    
  
}

void left()
{
  //Serial.println("left");
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
    analogWrite(ENA, 60);
    analogWrite(ENB, 60);
}

void right()
{
  //Serial.println("right");
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
    analogWrite(ENA, 60);
    analogWrite(ENB, 60);
}
